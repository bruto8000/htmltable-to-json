const fs = require("fs");
const HtmlTableToJson = require("html-table-to-json");

let htmlCode = fs.readFileSync("./index.html", "utf-8");
const jsonTables = HtmlTableToJson.parse(htmlCode);
let cities = jsonTables.results[0];
cities.forEach((cityObject) => {
  cityObject.city = cityObject["Город"];
  cityObject.subject = cityObject["Регион"];
});
cities = cities.map(({ city, subject }) => ({
  city,
  subject,
}));
fs.writeFileSync("./cities.json", JSON.stringify(cities, null, 2));
console.log("done");
